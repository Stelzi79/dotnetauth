﻿using System.Collections.Generic;
using System.Linq;

namespace DotNetAuth.Profiles
{
    public class LoginProvider
    {
        #region properties
        public LoginProviderDefinition Definition { get; set; }
        public string AppId { get; set; }
        public string AppSecret { get; set; }
        #endregion
        #region methods
        public OAuth1a.ApplicationCredentials GetOAuth1aCredentials()
        {
            return new OAuth1a.ApplicationCredentials { ConsumerKey = AppId, ConsumerSecret = AppSecret };
        }
        public OAuth2.ApplicationCredentials GetOAuth2Credentials()
        {
            return new OAuth2.ApplicationCredentials { AppId = AppId, AppSecretId = AppSecret };
        }
        #endregion

        #region static constructor
        static LoginProvider()
        {
            providers = new List<LoginProvider>();
        }
        #endregion
        #region static fields
        private static readonly List<LoginProvider> providers;
        #endregion
        #region static methods
        public static void SetKeys(LoginProviderDefinition provider, string appId, string appSecret)
        {
            SetKeys(provider.Name, appId, appSecret);
        }
        public static void SetKeys(string providerName, string appId, string appSecret)
        {
            var provider = Get(providerName);
            if (provider == null) {
                var providerDefinition = LoginProviderRegistry.Get(providerName);
                providers.Add(new LoginProvider { Definition = providerDefinition, AppId = appId, AppSecret = appSecret });
            }
            else {
                provider.AppId = appId;
                provider.AppSecret = appSecret;
            }
        }
        public static LoginProvider Get(string providerName)
        {
            return providers.FirstOrDefault(p => System.String.Compare(p.Definition.Name, providerName, System.StringComparison.OrdinalIgnoreCase) == 0);
        }
        #endregion
    }
}
