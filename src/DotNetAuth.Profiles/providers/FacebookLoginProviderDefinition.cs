namespace DotNetAuth.Profiles
{
    public class FacebookLoginProviderDefinition : LoginProviderDefinition
    {
        readonly ProfileProperty[] supportedProperties;
        public FacebookLoginProviderDefinition()
        {
            Name = "Facebook";
            Fullname = "Facebook";
            ProtocolType = ProtocolTypes.OAuth2;
            supportedProperties = new[] {
                ProfileProperty.UniqueID    .Result("id")           .Query("id"),
                ProfileProperty.BirthDate   .Result("birthday")     .Query("birthday")       .Scope("user_birthday"),
                ProfileProperty.DisplayName .Result("name"),
                ProfileProperty.Email       .Result("email")        .Query("email")          .Scope("email"),
                ProfileProperty.FirstName   .Result("first_name")   .Query("first_name"),
                ProfileProperty.FullName    .Result("name")         .Query("name"),
                ProfileProperty.Gender      .Result("gender")       .Query("gender"),
                ProfileProperty.LastName    .Result("last_name")    .Query("last_name"),
                ProfileProperty.Locale      .Result("locale"),
                ProfileProperty.Location    .Result("location.name").Query("user_location")  .Scope("user_location"),
                ProfileProperty.PictureLink .Result("picture")      .Query("picture"),
                ProfileProperty.ProfileLink .Result("link")         .Query("link"),
                ProfileProperty.Timezone    .Result("timezone")     .Query("timezone"),
                ProfileProperty.Username    .Result("username")     .Query("username"),
                ProfileProperty.Website     .Result("website")      .Query("user_website")   .Scope("user_website"),                
            };
        }
        public override ProfileProperty[] GetSupportedProperties()
        {
            return supportedProperties;
        }
        public override string GetProfileEndpoint(ProfileProperty[] requiredProperties)
        {
            var fields = "id,name,first_name,last_name,link,username,gender,timezone,birthday,email,user_location,picture,user_website";
            if (requiredProperties != null)
                fields = supportedProperties.GetFields(requiredProperties);
            return "https://graph.facebook.com/me?fields=" + fields;
        }
        public override string GetRequiredScope(ProfileProperty[] requiredProperties)
        {
            var requiredScope = "email,user_location,user_birthday,user_website";
            if (requiredProperties != null)
                requiredScope = supportedProperties.GetScope(requiredProperties);
            return requiredScope;
        }
        public override Profile ParseProfile(string content)
        {
            var json = Newtonsoft.Json.Linq.JObject.Parse(content);
            var locationObject = json.Property("location");
            var location = locationObject != null && locationObject.Value != null ? locationObject.Value.Value<string>("name") : null;
            var result = new Profile {
                UniqueID = json.Value<string>("id"),
                DisplayName = json.Value<string>("name"),
                FullName = json.Value<string>("name"),
                Email = json.Value<string>("email"),
                FirstName = json.Value<string>("first_name"),
                LastName = json.Value<string>("last_name"),
                ProfileLink = json.Value<string>("link"),
                PictureLink = json.Value<string>("picture"),
                Username = json.Value<string>("username"),
                Gender = json.Value<string>("gender"),
                Timezone = json.Value<string>("timezone"),
                BirthDate = json.Value<string>("birthday"),
                Location = location,
                Website = json.Value<string>("website"),
                Locale = json.Value<string>("locale")
            };
            return result;
        }
        public override OAuth2.OAuth2ProviderDefinition GetOAuth2Definition()
        {
            return new OAuth2.Providers.FacebookOAuth2();
        }
    }
}