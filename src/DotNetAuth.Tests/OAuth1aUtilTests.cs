﻿using System.Collections.Generic;
using System.Linq;
using DotNetAuth.OAuth1a.Framework;
using NUnit.Framework;

namespace DotNetAuth.OAuth1.Framework.Tests
{
    public class OAuth1aUtilTests
    {
        [TestFixture]
        public class GenerateNonceMethod
        {
            [Test]
            public void ShouldReturnABase64String()
            {
                var result = OAuth1aUtil.GenerateNonce();

                Assert.IsNotNullOrEmpty(result);
            }
            [Test]
            public void ShouldNotRepeatResultNonces()
            {
                var result1 = OAuth1aUtil.GenerateNonce();
                var result2 = OAuth1aUtil.GenerateNonce();

                StringAssert.AreNotEqualIgnoringCase(result1, result2);
            }
        }
        [TestFixture]
        public class CalculateParameterStringMethod
        {
            [Test]
            public void ShouldConcatValuesByAmpersand()
            {
                var parameters = new Dictionary<string, string>() { 
                    { "name1","value1"},
                    {"name2","value2"}
                };
                const string expected = "name1=value1&name2=value2";

                var result = OAuth1aUtil.CalculateParameterString(parameters.ToArray());

                Assert.AreEqual(expected, result);
            }
            [Test]
            public void ShouldEncodeKeys()
            {
                var parameters = new Dictionary<string, string>
                    {
                        {"name1!", "value1"}, // ! is not allowed, so should be escaped and be replaced by %21
                        {"name2~", "value2"} // ~ is allowed, so should not be escaped
                    };
                const string expected = "name1%21=value1&name2~=value2";

                var result = OAuth1aUtil.CalculateParameterString(parameters.ToArray());

                Assert.AreEqual(expected, result);
            }
            [Test]
            public void ShouldEncodeValues()
            {
                var parameters = new Dictionary<string, string>
                    {
                        {"name1", "value1!"}, // ! is not allowed, so should be escaped and be replaced by %21
                        {"name2", "value2~"} // ~ is allowed, so should not be escaped
                    };
                const string expected = "name1=value1%21&name2=value2~";

                var result = OAuth1aUtil.CalculateParameterString(parameters.ToArray());

                Assert.AreEqual(expected, result);
            }
        }
        [TestFixture]
        public class CalcualteSignatureBaseStringMethod
        {
            [Test]
            public void ShouldConcateArgumentsInCorrectOrder()
            {
                const string httpMethod = "GET";
                const string baseUrl = "http://test.com/";
                const string parametersString = "name1=value1&name2=value2";

                const string expected = "GET&http%3A%2F%2Ftest.com%2F&name1%3Dvalue1%26name2%3Dvalue2";

                var result = OAuth1aUtil.CalcualteSignatureBaseString(httpMethod, baseUrl, parametersString);

                Assert.AreEqual(expected, result);
            }
        }
        [TestFixture]
        public class GetSigningKeyMethod
        {
            [Test]
            public void ShouldConcateUsingAmpersand()
            {
                const string consumerSecret = "ConsumerSecret";
                const string expected = "ConsumerSecret&OAuthTokenSecret";

                var result = OAuth1aUtil.GetSigningKey(consumerSecret, "OAuthTokenSecret");

                Assert.AreEqual(expected, result);
            }
            [Test]
            public void IfOAuthTokenSecretIsMissingStillAddsAmpersand()
            {
                const string consumerSecret = "ConsumerSecret";
                const string expected = "ConsumerSecret&";

                var result = OAuth1aUtil.GetSigningKey(consumerSecret);

                Assert.AreEqual(expected, result);
            }
            [Test]
            public void IfOAuthTokenSecretIsNullStillAddsAmpersand()
            {
                const string consumerSecret = "ConsumerSecret";
                string OAuthTokenSecret = null;
                const string expected = "ConsumerSecret&";

                var result = OAuth1aUtil.GetSigningKey(consumerSecret, OAuthTokenSecret);

                Assert.AreEqual(expected, result);

            }
        }
    }
}