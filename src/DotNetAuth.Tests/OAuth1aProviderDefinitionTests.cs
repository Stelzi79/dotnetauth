using System;
using System.Linq;
using DotNetAuth.OAuth1a;
using NUnit.Framework;

namespace DotNetAuth.OAuth1.Framework.Tests
{
    public class OAuth1aProviderDefinitionTests
    {
        [TestFixture]
        public class GetRequestTokenParametersMethod
        {
            public class TestOAuth1aProviderDefinition : OAuth1aProviderDefinition
            {

            }

            ApplicationCredentials credentials;
            OAuth1aProviderDefinition definition;

            [NUnit.Framework.SetUp]
            public void Setup()
            {
                credentials = new ApplicationCredentials { ConsumerKey = "consumerkey", ConsumerSecret = "consumersecret" };
                definition = new TestOAuth1aProviderDefinition();
            }

            [Test]
            public void ShouldUseCreadentialsConsumerKey()
            {
                var parameters = definition.GetRequestTokenParameters(credentials, "");
                var parameter = parameters.ToList().Single(p => p.Name == "oauth_consumer_key");

                Assert.NotNull(parameter, "Consumer key is not added to parameters, it is mandatory.");
                Assert.AreEqual(credentials.ConsumerKey, parameter.Value, "Consumer key is different from credentials.");
            }
            [Test]
            public void ShouldNotUseCreadentialsConsumerSecret()
            {
                var parameters = definition.GetRequestTokenParameters(credentials, "");

                var consumerSecretIsUsed = parameters.ToList().Any(p => p.Value == credentials.ConsumerSecret);

                Assert.False(consumerSecretIsUsed, "Consumer Secret may not be used in request token request.");
            }
            [Test]
            public void ShouldAddParameterVersionWithExactValueOf1point0()
            {
                var parameters = definition.GetRequestTokenParameters(credentials, "");

                var versionParameter = parameters.ToList().Single(p => p.Name == "oauth_version");
                Assert.NotNull(versionParameter, "Version parameter is not added but it is mandatory.");
                Assert.AreEqual("1.0", versionParameter.Value, "Invalid value for version. Version parameter is expected to always have value of 1.0");
            }
            [Test]
            public void TimestampParametersShouldBeSecondsSinceJanuary_1_1970_GMT()
            {
                var expected = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;

                var parameters = definition.GetRequestTokenParameters(credentials, "");

                var timestampParameter = parameters.ToList().Single(p => p.Name == "oauth_timestamp");
                if (timestampParameter == null)
                    Assert.Inconclusive("This test checks the value of timestamp to be valid.");

                int timestampAsSeconds = -1;
                try {
                    timestampAsSeconds = int.Parse(timestampParameter.Value);
                }
                catch {
                    Assert.Fail("timestamp value should be a positive integer");
                }

                var distance = timestampAsSeconds - expected;

                Assert.AreEqual(expected, timestampAsSeconds, 1, "Timestamp value seems to be invalid.");
            }
            [Test]
            public void SignatureValueShouldBeAValidOne()
            {
                var validValues = new[] { "HMAC-SHA1", "RSA-SHA1", "PLAINTEXT" };

                var parameters = definition.GetRequestTokenParameters(credentials, "");
                var parameter = parameters.ToList().Single(p => p.Name == "oauth_signature_method");

                if (parameter == null) {
                    Assert.Inconclusive();
                }

                Assert.That(validValues.Contains(parameter.Value), "The value for signature method is not valid.");
            }
        }

    }
}
